#!/bin/sh
set -euf

mkdir -p build
mkdir -p dist

npm run build
echo "$(cat header.html)\n$(cat build/bylaws.html)" > dist/bylaws.html

rm -r build

