DSA San Francisco Bylaws
Ratified April 26th, 2017 / Amended January 24th, 2018

# Article I. Name
The name of this organization shall be the Democratic Socialists of America, San Francisco (“DSA SF”), a local chapter (“Chapter”) of the Democratic Socialists of America (“DSA”). The name is taken from the city and county of San Francisco, California (“San Francisco”) that the Chapter will serve.

# Article II. Purpose
## Section 1. Vision
DSA SF’s vision is a United States that is liberated from the control and corruption of capitalism in our economy, society, and community. Instead of a political-economic system which benefits an elite few, we envision a social order based on democratic control of resources and production, economic planning, equitable distribution, and social and economic justice for all. We envision San Francisco as an exemplar of these values.

## Section 2. Mission
DSA SF’s mission is to organize the people of San Francisco in order to establish and exercise collective power that can fight capitalism, combat inequality, and bring about real socialist change.

## Section 3. Objectives
1. Build collective power that can stand up to capital.
2. Actively organize throughout San Francisco to exhibit solidarity, build stronger communities, and bring about real material change for the people.
3. Educate ourselves and the people of San Francisco on socialist critiques of capitalism and advocate for democratic socialist values, vision, and policy.
4. Act as a San Francisco hub for DSA National events, projects, and campaigns.
5. Partner with, contribute to, and support adjacent DSA Chapters in the San Francisco Bay Area, California.

# Article III. Membership

## Section 1. Qualifications
Membership shall be open to dues-paying members of the DSA whom reside in or hold employment in San Francisco or the larger San Francisco Bay Area and are not members of another DSA Chapter.

## Section 2. Dues
Annual dues are set by the DSA National and must be paid to the DSA National office. Voluntary member dues may be established on behalf of DSA SF, but their nonpayment may not restrict membership status or eligibility to serve on any committees or working groups.

## Section 3. “Good Standing”
Members are considered in “good standing” provided that they are considered in good standing with DSA National.

## Section 4. “Active”
Members are considered “active” provided they have attended two of the three most recent “Regular Meetings” or consistently participate in Working Groups or Committees, as testified by nomination by a chair of a Working Group or Committee they are in.

## Section 5. Suspension and Expulsion
Members may be recommended for expulsion or suspension from the Chapter by a two-thirds vote of the Steering Committee provided they are found to be in substantial disagreement with the principles or policies of the organization; they consistently engage in undemocratic, disruptive behavior; they publicly misrepresent the formal endorsements or positions adopted by the Chapter pursuant to Article XII; or they are under the discipline of any self-defined democratic-centralist organization. The recommended disciplinary action must be ratified at a Special Meeting, provided at least 15 days notice has been given to the membership by a simple majority vote of the members present. Members facing expulsion must receive written notice of the charges against them and must be given the opportunity to speak before both the Steering Committee and the members at the Special Meeting prior to the aforementioned vote.

## Section 6. Sexual Harassment and Anti-Discrimination Policy
Sexual harassment and discrimination on the basis of such inappropriate grounds as age, disability, national origin, race, religion, gender or sexual identity or orientation are antithetical to the principles of democratic socialism and to the vision and mission of DSA SF. A member determined to have engaged in sexual harassment or inappropriate discrimination shall be deemed to be in substantial disagreement with the principles or policies of DSA SF and may be considered for suspension or expulsion under Article III Section 5. 

DSA SF may adopt a formal policy that defines sexual harassment and inappropriate discrimination for purposes of this section and/or prescribes procedures for resolving allegations of sexual harassment or inappropriate discrimination.

# Article IV. Steering Committee

## Section 1. Members
The Officers of DSA SF along with any at-large members and representatives of branches shall compose the Steering Committee. 

## Section 2. Responsibilities
* The Steering Committee shall meet as a whole at least once per quarter (in person or by conference call). Those meetings shall be open to chapter members unless the Committee goes into executive session by two-thirds vote of the committee.
* The Steering Committee shall concern itself primarily with establishing program activities and proposing guidelines for consideration by the general membership.
* The Steering Committee shall be responsible for providing the organizational infrastructure required for DSA SF’s meetings and for members to accomplish projects and tasks within working groups and committees.
* The Steering Committee, working with relevant committees, must publish annual reports describing the planned projects and accomplishments of DSA SF to members 15 days prior to General Meeting.
* The Steering Committee is responsible for organizing efforts to satisfy DSA’s priorities within San Francisco, as determined by the general membership.
* The Steering Committee shall be responsible for presenting budgetary appropriations to the members at Regular Meetings, to be approved by a majority vote of members present.
* The Steering Committee shall be responsible for acting on the organization's behalf between Regular and General Meetings.

## Section 3. Quorum.
A quorum of three members or fifty percent, whichever is higher, is required for a valid meeting of the Steering Committee. A Steering Committee member may appoint a proxy who is not already a voting member of the Steering Committee to vote on the member’s behalf when necessary. Unless otherwise specified in the bylaws, all votes by the Steering Committee require a simple majority of votes cast.

# Article V. Officer Positions and Duties

## Section 1. Definition
The officers of DSA SF will be the two Co-Chairs, Vice Chair, Secretary, Treasurer and all at-large members. All officers shall perform the duties prescribed by these Bylaws and the standing rules defined throughout the lifetime of DSA SF. No officer may hold more than one officer position at a time. Officers will serve for one-year terms or until successors are elected. 

## Section 2. Diversity
At least two-fifths of the officers must self-identify as women; and at least two-fifths must be people of color. If the elected officers of DSA SF do not meet these requirements, an appropriate Branch, Committee, or Working Group (as determined by motion from the floor of the General Assembly) must nominate additional at-large members of the Steering Committee to fulfill these requirements, who may be confirmed by a majority vote of the general assembly.

## Section 3. Co-Chairs and Vice Chair
The Co-Chairs shall be the chief spokespeople of the organization. They shall manage the arranging facilitation of all meetings of the organization and of the Steering Committee, shall have responsibility for overall direction and management of the organization, and shall interpret the Constitution and Bylaws, subject to appeal to the Steering Committee. The Co-Chairs must not both identify as male. The Vice Chair shall act as deputy to either Co-Chair.

## Section 4. Secretary
The Secretary shall be responsible for maintaining accurate membership lists, keeping minutes and records of “Regular” and “General Meetings” and maintaining communication channels utilized by DSA SF. The Secretary shall also be responsible for notifying the Steering Committee and the membership of all meetings.

## Section 5. Treasurer
The Treasurer shall have custody of all funds of the organization and shall be responsible for the financial management of the organization. The Treasurer shall be responsible for presenting regular budgetary updates at Steering Committee meetings, as well as an end-of-year financial statement. The Treasurer shall be responsible for authorizing all expenditures in accordance with the wishes of the Steering Committee (in the case of expenditures over $100, prior approval must be sought from the Steering Committee), and shall organize dues collection, should DSA SF decide to collect dues.

## Section 6. At-large members
Branch representatives and elected representatives not selected for an officer position are considered “At-large” representatives.

## Section 7. Candidacy and Elections
* Candidates for the Steering Committee must be active members in good standing with DSA SF and not under the discipline of any self-defined democratic-centralist organization.
* Every candidate is simply running to be on the Steering Committee, not for any specific position.
* Candidates must be nominated by two members in good standing at the general meeting before the election is to be held.
* At the meeting where the election is held, every valid candidate is given equal time to speak. Voters then rank their choices for candidates.
* Elections for the Steering Committee will be held according to the rules set forth in [Article XII|#article-xii-elections].
* Officers are determined as follows:
  1. In total, five Steering Committee members shall be elected through the process described in Article XII.
  2. The first candidate elected by the method described is one of the Chairs.
  3. The next candidate elected by that method who does not identify as the same gender as the first chair is their co-chair. If no candidate elected qualifies, then the selected Chair from step i chooses their co-chair from the appointed at-large delegates.
  4. The remaining officers are determined by vote from within the newly constituted Steering Committee.
    * The Vice Chair is selected from among the committee members.
    * The Secretary & Treasurer MAY be selected from among the committee members. However, if (and only if) none of the committee wish to take the role, then the roles may be appointed from the general membership. At that point said member assumes the title, duties, and authorities of that role. They also become a non-voting member of the Steering Committee.

## Section 8. Acting Officers
Should an officer leave DSA SF, resign the position, or be removed from office, the Steering Committee may select an acting officer to hold the position until a Special Meeting can be held to conduct an election within the next 60 days.

## Section 9. Removal of an Officer
Officers may be removed by a referendum vote of the Local’s members with a majority result sufficient for removal. The referendum vote may be called forth by a motion of no confidence by any Steering Committee member. General membership must be given at least two week’s notice of the upcoming removal vote. 

# Article VI. Treasury
The treasury must be deposited in a local credit union; corporate banking institutions are forbidden unless no credit union is available. Local expenditures out of the treasury must prefer contracts with union labor. Corporate donations are forbidden to be accepted by the treasury.

# Article VII. Committees and Working Groups

## Section 1. Definition and Purpose
In order to more effectively organize to achieve its mission and objectives, DSA SF may establish committees and working groups. Committees may be established for the purpose of organizing around and/or performing specific functions for the chapter (e.g., design, marketing and communication, public outreach, recommending endorsements, etc.). Working groups may be established for the purpose of organizing action around political themes or projects (e.g., housing, immigration, feminism, reading groups, etc.) that fall under the purview of the DSA and/or DSA SF’s political platform.

## Section 2. Creation
Members who wish to form a committee or working group must submit a proposed charter that includes (1) a group name; (2); mission statement and objectives; (3) initial strategy for achieving the stated objectives; (4) list of at least five prospective members (including two who are designated as Co-Chairs) signed by those members; and (5) if it is anticipated that the proposed committee or working group will seek authorization to expend the chapter’s funds, a short explanation of the anticipated use of those funds and estimate of the amount to be spent.

Proposed charters for committees, but not working groups, must additionally include (6) a statement of whether the proposed committee is to be a standing committee of indefinite duration or a temporary committee to expire at a time certain or upon the occurrence of a specified event; (7) whether membership in the committee is to be open to all members in good standing or limited; and (8) if membership is to be limited, the number of members the committee shall have and the means by which members will be selected.

A proposed committee or working group shall be recognized and chartered with the approval of a majority of the votes cast at a Regular, Special, or General meeting. Charters for proposed committees and working groups must be published on the agenda or announced to the membership at least 15 days prior to the meeting.

## Section 3. Membership and Election of Co-Chairs
Membership in a committee shall be open to all DSA SF members in good standing unless the committee’s charter limits membership, in which case the terms of the charter shall control. Committees shall maintain an official membership list.

Membership in working groups shall be open to all DSA SF members in good standing. Working groups are encouraged to maintain an official membership list, but are only required to do so to maintain eligibility to make authorized expenditures under Section 5, below.

The founders of a new committee or working group may designate two temporary co-chairs. Temporary co-chairs will serve until the committee or working group is ratified; an election for committee leadership must then be held at no later than the second meeting following ratification. It is recommended that committees use the same Single Transferable Voting system that DSA SF uses for chapter-wide elections, especially for contested committee elections. Ratified committees and working groups must elect two co-chairs and may also elect a vice-chair to serve as their deputy. Terms will last either 6 months or one year. Unless no candidates who do not identify as male choose to stand in the election, at least one of the co-chairs elected must not identify as male.

All co-chairs and vice chairs must be added to current leadership channels and listservs. All co-chairs and vice-chairs must voluntarily leave said channels and listservs after their terms are over if they are not re-elected.

In the interest of sharing leadership opportunities and elevating rank and file members, co-chairs of committees and working groups (including steering committee) cannot chair more than one committee or working group unless no other eligible members wish to run.

## Section 4. Duties and Responsibilities
The members of a committee or working group are responsible for executing their respective body’s strategy in fulfillment of its objectives. Co-Chairs are responsible for organizing their respective body’s members, running meetings, acting as liaisons to the Steering Committee, and serving as the body’s points of contact to the DSA SF membership.

Committees must keep official records of meeting dates, attendance, and meeting notes, which must be made available to the DSA SF membership. Working groups are encouraged to maintain these records, but are only required to do so to maintain eligibility to make authorized expenditures under Section 5, below.

## Section 5. Expenditures
Committees, as well as working groups that maintain an official membership list and official records of meeting dates, attendance, and meeting notes, may seek authorization to expend the organization’s funds if necessary to accomplish their objectives. All requests for authorization must be presented to the Treasurer and Steering Committee and must be approved by the Steering Committee. A committee or working group shall maintain records of all authorized expenditures made using the organization’s funds and shall provide these records to the Treasurer.

## Section 6. Review, Modification, and Dissolution
A committee’s or working group’s activities shall be reviewed by the Steering Committee at the Regular Steering Committee meetings. Committees and working groups shall report on their activities to the DSA SF membership at one Regular meeting each quarter or more frequently as necessary.

Existing committees and working groups may amend their charters with the approval of a majority of the votes cast at a Regular, Special, or General meeting. Proposed amendments to a committee’s or working group’s charter must be published on the agenda or announced to the membership at least 15 days prior to the meeting.

Committees that are chartered as temporary committees shall dissolve automatically upon the expiration of the certain time or occurrence of the particular event specified in their charter. All other committees shall continue in existence indefinitely unless dissolved by a majority of the votes cast at a Regular, Special, or General meeting. Working groups shall also continue in existence indefinitely unless dissolved by a majority of the votes cast at a Regular, Special, or General meeting, or by the resignation or unanimous consent of the working group’s active members.

Upon dissolution, the member(s) maintaining custody of the committee’s or working group’s records shall provide copies of those records to the Secretary.

# Article VIII. Youth Sections

## Section 1. Branch Status
Chapters of the Young Democratic Socialists located in San Francisco shall automatically be considered branches of DSA SF.

# Article IX. Organizing Body and Meetings

## Section 1. Members
The members of the organization (or “the membership”), meeting in General Meeting or in Special Meeting, shall be the highest body of the organization. Special Meetings are restricted in scope and shall have the authority to deal with only those specific matters for which they may be called. The Steering Committee shall be the highest body of the organization between General, Regular and Special Meetings, and shall be responsible for the administration of the organization and the implementation of policies formulated by members.

## Section 2. General Meetings
DSA SF shall hold a General Meeting annually with at least 30 days notice given to all members. The members shall meet to elect officers and to discuss and decide primarily, but not exclusively, the political orientation of the organization and program direction.

## Section 3. Special Meetings
By a majority vote of the Steering Committee or a petition of fifteen members, a Special Meeting can be called, with notice given to all members at least 15 days prior to the meeting. The call to the Special Meeting shall specify the matters to be discussed therein and during the meeting, no other matter may be brought to the floor.

## Section 4. Regular Meetings
DSA SF shall hold regular meetings at a frequency of at least once per quarter, and up to once per month. The agenda for a Regular Meeting shall include reports from the Steering Committee and any other committee that has met since the last meeting, reports from any working groups with relevant updates, political discussion, budgetary appropriations, and education as well as open time for members to raise items not included on the meeting agenda.

## Section 5. Voting
Every active member in good standing of the chapter shall have voting rights at General, Special and Regular Meetings. Members shall have the ability to renew DSA dues at the start of the General Meeting.

## Section 6. Quorum
A quorum of ten percent of active membership or fifteen active members, whichever is greater, shall be required for valid General, Special and Regular Meetings.

# Article X. Endorsements, Event Sponsorships, and Online Voting
## Section 1. Formal Endorsements and Event Sponsorships
Candidates running for local, state, and national political office may be formally endorsed by DSA SF as a whole at a general, regular, or special meeting. The endorsement vote must be published on the agenda or announced to the membership at least two weeks prior to the meeting. A sixty percent majority of the votes cast will be required for DSA SF to endorse any candidate.

In addition to endorsing candidates for office, DSA SF may also choose to formally endorse particular positions on local, state, and national issues of interest to the chapter, or sponsor events, protests, or direct actions so long as the positions, events, protests, or direct actions are relevant to and consistent with the vision, mission, and objectives of DSA SF as set forth in Article II.  Such positions may be endorsed, or events, protests, or direct actions sponsored, by DSA SF as a whole at a general, regular, or special meeting. The endorsement or sponsorship vote must be published on the agenda or announced to the membership at least two weeks prior to the meeting. A sixty percent majority of the votes cast will be required for DSA SF to endorse any position or sponsor any event, protest, or direct action.

In addition, the Steering Committee has the power to sponsor events, protests, or direct actions (but not to endorse positions or candidates) at any time by a vote of ⅘ of its members. However, it may not vote to sponsor events, protests, or direct actions if doing so would conflict with or be inconsistent with a prior chapter vote, and it must report any such decisions back to the chapter as soon as reasonably possible, but by no later than at the next general or regular meeting, whichever is earliest. If the sponsored event, protest, or direct action has not yet taken place at the time of that meeting, any member in good standing may move from the floor to overturn the sponsorship by a simple majority of the votes cast. This paragraph shall expire and be automatically removed at the conclusion of the 2018 election of the full Steering Committee unless this sentence is removed (and the paragraph thus made permanent) by a simple majority vote of the chapter, which may take place at a meeting or by online vote.

## Section 2. Online Voting
The steering committee may decide, at the request of any member making such a proposal, to hold an online vote on any issue except for candidate or ballot initiative endorsements. Members shall be eligible to vote so long as they are active members in good standing, as defined in Article III Section 4, as of the start of the online voting period following close of debate. At least 50% of active members in good standing must vote or the vote will be considered invalid for lack of a quorum.
The steering committee shall announce all online votes to the chapter by email and all other reasonable means. There shall be a 72-hour period for debate following the announcement. At the close of debate, there shall be a 48-hour voting period during which any eligible member may vote. No motions to call the question or extend debate will be allowed, and no amendments will be allowed. However, at any time after the start of debate, any eligible member may motion to table the proposal with a simple majority vote.

All voting thresholds for a passing vote shall be the same as if the vote were held at an in-person meeting. Votes to abstain will count toward the quorum. A vote can become binding before the close of the 48-hour voting period if the number of votes in favor or against has surpassed the threshold that would be required if all eligible votes were cast.

## Section 3. Ban on Misrepresentation of Formal Endorsements
No officer or member of DSA SF shall publicly misrepresent the Chapter’s formal endorsements or positions.  Intentional or willful public misrepresentation of the Chapter’s endorsements or positions is grounds for suspension or expulsion under Article III Section V. 

# Article XI. National Delegates

## Section 1. Selection of Delegates
Delegates to the National DSA Convention shall be elected by the membership. In accordance with the national bylaws, this election shall be held no earlier than four months, and no later than forty-five days, prior to the opening of the National Convention. No election for delegates shall be conducted before the apportionment of delegates has been received from the National DSA.

Elections shall be held, with 30 days notice, according to the procedure and rules specified in Article XII.

# Article XII. Elections

## Section 1. Nominee Campaigns
Campaigns for DSA SF offices shall be limited to the 30 day window between notice of the Regular Meeting and the election meeting itself. All Nominees will have equal opportunity to address the membership at the meeting.

## Section 2. Process
Elections for DSA SF leadership and delegate positions may be held at a General, Regular, or Special Meeting. All candidates shall be elected at the meeting through secret ballot by active members in good standing. Ballots must be counted immediately following the final ballot being cast. Ballots must be counted twice by a group between two and five active members in good standing in full unobstructed view of any interested Members acting as election observers. All reasonable efforts should be made to find counters that are acceptable to all candidates.

## Section 3. Ranked Choice Voting
All elections in the chapter will use ranked choice voting. "Ranked Choice Voting" means a method of casting and tabulating votes that simulates the ballot counts that would occur if all voters participated in a series of runoff elections with one candidate eliminated after each round of counting. In elections using the Ranked Choice Voting method, voters may rank the candidates in order of preference.

## Section 4. Ballot Count
Votes are tallied by the Scottish STV method, as described here. Candidates are elected according to this count procedure in the order that they receive the requisite votes.

# Article XIII. Dissolution
In the event of the dissolution of DSA SF, all remaining funds and assets are to be released to the nearest DSA chapter most likely to inherit its members or the DSA National when a nearby chapters cannot be found.

# Article XIV. General Provisions

## Section 1. Interpretation
These bylaws shall be interpreted by the Co-Chairs, subject only to appeal by the Steering Committee or by a resolution at the General or Special Meeting. All powers not delegated to committees or working groups either defined in these bylaws or granted through vote of the membership are reserved to the membership.

## Section 2. Rules
The rules contained in the current edition of Robert’s Rules of Order Newly Revised shall govern the organization in all cases to which they are applicable and in which they are not inconsistent with these Bylaws or standing rules of DSA SF.

## Section 3. Notifications
All requirements for giving notice to the membership should be met by email and public post to the DSA SF website.

# Article XV. Amendments

## Section 1. Proposals
Written proposals for amendments to the Bylaws dictated here, signed by at least three co-sponsors, may be submitted to the Steering Committee at a Steering Committee, Regular, or General Meeting. The Steering Committee shall promptly distribute the amendment text for review by the membership. 

## Section 2. Ratification
The proposal may be ratified by a two-thirds vote of Members present at a Regular or General Meeting, or a Special Meeting called for that purpose. The ratification vote must take place not less than two weeks, but not more than six weeks, following the distribution of the proposal.

