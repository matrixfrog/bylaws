# bylaws

San Francisco DSA bylaws

## Generating Markdown

BYLAWS.md was originally copied from google docs. Headers appear on their own 
line, so basic formatting can be achieved with two find/replace commands in vim.

```
%s/^Article/\r# Article/
%s/^Section/## Section/
```

Unfortunately (or fortunately?), bulleted or numeric lists are not represented
in the copy/paste from google docs. These will need to be handled specially.


